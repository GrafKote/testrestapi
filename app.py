from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
from flask_uuid import FlaskUUID
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


app = Flask(__name__)


FlaskUUID(app)
CORS(app)

app.config['CORS_HEADERS'] = 'Content-Type'
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql+psycopg2://nikita:78372@192.168.1.201:5432/tenderapi"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)
migrate = Migrate(app, db)

Base = db.Model

from models import *


@app.route('/tasks', methods=['GET'])
@cross_origin()
def get_tasks():
    tasks = Tasks.query.all()
    serialized = []
    for task in tasks:
        serialized.append({
            'id': task.id,
            'uuid': task.uuid,
            'pars_query': {
                'q': task.q,
                'q_date': task.q_date,
            },
            'task_date': task.task_date,
            'parsers': task.parsers.split(','),
            'status': task.status
        })
    response = jsonify(serialized)
    return response


@app.route('/tasks/<uuid:task_id>', methods=['GET'])
@cross_origin()
def get_task(task_id):
    item = Tasks.query.filter(Tasks.uuid == task_id).first()
    serialized = {
        'id': item.id,
        'uuid': item.uuid,
        'pars_query': {
            'q': item.q,
            'q_date': item.q_date,
        },
        'task_date': item.task_date,
        'parsers': item.parsers.split(','),
        'status': item.status
    }
    response = jsonify(serialized)
    return response


@app.route('/tasks', methods=['POST'])
@cross_origin()
def create_tasks():
    res = request.json
    print(res)
    new_task = Tasks(**{
        'uuid': res["uuid"],
        'q': res["pars_query"]["q"],
        'q_date': res["pars_query"]["q_date"],
        'task_date': res["task_date"],
        'parsers': ','.join(res["parsers"]),
        'status': res["status"]
    })
    db.session.add(new_task)
    db.session.commit()
    serialized = {
        'id': new_task.id,
        'uuid': new_task.uuid,
        'pars_query': {
            'q': new_task.q,
            'q_date': new_task.q_date,
        },
        'task_date': new_task.task_date,
        'parsers': new_task.parsers.split(','),
        'status': new_task.status
    }
    response = jsonify(serialized)
    return response


@app.route('/tasks/<uuid:task_id>', methods=['PUT'])
@cross_origin()
def update_task(task_id):
    item = Tasks.query.filter(Tasks.id == task_id).first()
    params = request.json

    if not item:
        return {'message': 'No task whit this id'}, 400
    for key, value in params.items():
        setattr(item, key, value)
    db.session.commit()
    serialized = {
        'id': item.id,
        'uuid': item.uuid,
        'pars_query': {
            'q': item.q,
            'q_date': item.q_date,
        },
        'task_date': item.task_date,
        'parsers': item.parsers.split(','),
        'status': item.status
    }
    response = serialized
    return response


@app.route('/tasks/<uuid:task_id>', methods=['DELETE'])
@cross_origin()
def delete_task(task_id):
    print(task_id)
    item = Tasks.query.filter(Tasks.uuid == task_id).first()
    if not item:
        return {'message': 'No task whit this id'}, 400
    db.session.delete(item)
    db.session.commit()
    return '', 204


@app.teardown_appcontext
def shutdown_session(exception=None):
    db.session.remove()


if __name__ == '__main__':
    app.run(debug=False)
