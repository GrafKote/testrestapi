from app import db, Base


class Tasks(Base):
    __tablename__ = 'tasks'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    uuid = db.Column(db.Integer, nullable=False, unique=True)
    q = db.Column(db.String(250), nullable=False)
    q_date = db.Column(db.String(250), nullable=False)
    task_date = db.Column(db.String(250), nullable=False)
    parsers = db.Column(db.String(250), nullable=False)
    status = db.Column(db.Boolean, nullable=False)