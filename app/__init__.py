from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
from flask_uuid import FlaskUUID
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import os, config


app = Flask(__name__)
app.config.from_object(os.environ.get('FLASK_ENV') or 'config.DevelopementConfig')

FlaskUUID(app)
CORS(app)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
Base = db.Model

from . import views